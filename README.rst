=============
报喜鸟微信企业号 
=============

基于微信企业号SDK开发

.. image:: https://img.shields.io/badge/built%20with-Django 2.0.13-ff69b4.svg
     :target:  https://gitlab.com/JulianYangJingJun/vip-marketing
     :alt: Built with Django 2.0.13

.. image:: https://img.shields.io/badge/built-Python 3.7-2b5b84.svg
      :target: https://www.python.org/
      :alt: Built Python 3.7

.. image:: https://img.shields.io/badge/Co.,Ltd.-%E6%8A%A5%E5%96%9C%E9%B8%9F-red.svg
     :target:  http://www.baoxiniao.com/
     :alt: Built with Django

:License: MIT
 
开发环境部署基本命令
-----------------
一、文件 $HOME/.env 基本配置

1. DATABASE_PURL  生产环境 数据库 配置

2. DATABASE_LURL  开发环境 数据库 配置 

3. WX_CORP_ID  微信企业号 corp_id

4. WX_API_URL 微信企业号 API url

二 、基本命令

数据迁移
     :: $ export python manage.py makemigrations

     :: $ export python manage.py migrate

开发运行
     :: $ export python manage.py runserver

